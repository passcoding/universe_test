//
//  LimitingProvider.swift
//  Test
//
//  Created by Dmytro Pasinchuk on 30.06.2020.
//  Copyright © 2020 Universe. All rights reserved.
//

import RxSwift

class LimitingProvider {
    private let semaphore: DispatchSemaphore
    
    init(taskLimitation: Int) {
        self.semaphore = DispatchSemaphore(value: taskLimitation)
    }

    func limitTasks<T>(_ observable: Observable<T>) -> Observable<T> {
        let selfSemaphore = semaphore
        return Observable.create { observer in
            selfSemaphore.wait()
            let disposable = observable.subscribe { event in
                selfSemaphore.signal()
                switch event {
                case .next:
                    observer.on(event)
                case .error, .completed:
                    observer.on(event)
                }
            }
            return Disposables.create {
                disposable.dispose()
            }
        }
    }
}
