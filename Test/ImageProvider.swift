//
//  ImageProvider.swift
//  Test
//
//  Created by Alexey Savchenko on 26.06.2020.
//  Copyright © 2020 Universe. All rights reserved.
//

import RxSwift

class ImageProvider {
  func getImageData(index: Int) -> Single<Data> {
    return Single<Data>.create { single in
        var imageDataForSaving: Data? = nil
        autoreleasepool {
            let renderSize = CGSize(width: 2000, height: 2000)
            let rect = CGRect(origin: .zero, size: renderSize)
            let renderer = UIGraphicsImageRenderer(size: renderSize)
            let data = renderer.jpegData(withCompressionQuality: 1.0) { (ctx) in
              UIColor.white.setFill()
              let circlePath = UIBezierPath(ovalIn: rect)
              ctx.cgContext.setLineWidth(10)
              ctx.cgContext.addPath(circlePath.cgPath)
              UIColor.red.setStroke()
              circlePath.stroke()
              
              let string = NSString(string: "\(index)")
              string.draw(at: .init(x: rect.midX, y: rect.midY),
                          withAttributes: [.foregroundColor: UIColor.black,
                                           .font: UIFont.systemFont(ofSize: 30, weight: .bold)])
              print("Create image with index: \(index)")
            }
            
            imageDataForSaving = data
        }
        if let validData = imageDataForSaving {
            single(.success(validData))
        } else {
            let error = NSError(domain: "ImageProvider", code: 100, userInfo: nil)
            single(.error(error))
        }
      
        return Disposables.create()
    }
  }

  
}
