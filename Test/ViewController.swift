//
//  ViewController.swift
//  Test
//
//  Created by Alexey Savchenko on 26.06.2020.
//  Copyright © 2020 Universe. All rights reserved.
//

import UIKit
import RxSwift

class ViewController: UIViewController {
    
  private var viewModel: ImageProcessingViewModel?
  
  private let disposeBag = DisposeBag()
    
    init(viewModel: ImageProcessingViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        processImages()
    }
    private func processImages() {
        guard let viewModel = viewModel else { return }
        viewModel.processedImagesResults
            .subscribe(onSuccess: { results in
                let saveResult = results.reduce(into: (0, 0)) { (counter, result) in
                    switch result {
                    case .failure:
                        counter.0 += 1
                    case .success:
                        counter.1 += 1
                    }
                }

                print("Operation complete. Failed - \(saveResult.0); Saved - \(saveResult.1)")
            }, onError: { error in
                print(error)
            })
            .disposed(by: disposeBag)
    }
}
