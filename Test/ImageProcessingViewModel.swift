//
//  ImageProcessingViewModel.swift
//  Test
//
//  Created by Dmytro Pasinchuk on 08.07.2020.
//  Copyright © 2020 Universe. All rights reserved.
//

import RxSwift

class ImageProcessingViewModel {
    private let provider: ImageProvider
    private let saver: ImageSaver
    private let limitingProvider: LimitingProvider
    private let processingScheduler: SchedulerType
    
    init() {
        self.provider = ImageProvider()
        self.saver = ImageSaver()
        self.limitingProvider = LimitingProvider(taskLimitation: 4)
        self.processingScheduler = ConcurrentDispatchQueueScheduler(qos: .userInitiated)
    }
    
    var processedImagesResults: Single<[Result<Void, Error>]> {
        return processImages()
    }
    
    private func processImages() -> Single<[Result<Void, Error>]> {
        let imagesIndexes = (0..<1000)
        return  Observable
                    .from(imagesIndexes)
                    .flatMap { [unowned self] index in
                        self.provider.getImageData(index: index).subscribeOn(self.processingScheduler)
                    }
                    .flatMap { [unowned self] imageData in
                        return self.saver.saveImageData(imageData).subscribeOn(self.processingScheduler)
                    }
                    .toArray()
    }
}
