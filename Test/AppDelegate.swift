//
//  AppDelegate.swift
//  Test
//
//  Created by Alexey Savchenko on 26.06.2020.
//  Copyright © 2020 Universe. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?
  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    window = UIWindow(frame: UIScreen.main.bounds)
    let imageProcessingViewModel = ImageProcessingViewModel()
    window?.rootViewController = ViewController(viewModel: imageProcessingViewModel)
    window?.makeKeyAndVisible()
    
    return true
  }
}
