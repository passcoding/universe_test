//
//  ImageSaver.swift
//  Test
//
//  Created by Alexey Savchenko on 26.06.2020.
//  Copyright © 2020 Universe. All rights reserved.
//

import RxSwift

class ImageSaver {
    func saveImageData(_ imageData: Data) -> Single<Result<Void, Error>> {
      return Single<Result<Void, Error>>.create { single in
          let id = UUID()
          let url = FileManager.default
            .urls(for: .documentDirectory, in: .userDomainMask)[0]
            .appendingPathComponent(id.uuidString)
            .appendingPathExtension("jpeg")
          do {
            try autoreleasepool {
                try imageData.write(to: url)
            }
            print("Write image with id: \(id)")
            single(.success(.success(())))
          } catch {
            single(.error(error))
            }
         return Disposables.create()
        }
    }
}
